<?php namespace Bedard\FormBuilder\Models;

use Model;

/**
 * Field Model
 */
class Field extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'bedard_formbuilder_fields';

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'form_id',
        'position',
        'name',
        'label',
        'span',
        'comment',
        'type',
        'options',
        'placeholder',
        'default',
        'validation',
        'validation_message',
        'validation_regex',
        'is_checked',
        'is_required'
    ];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'form' => ['Bedard\FormBuilder\Models\Form', 'table' => 'bedard_formbuilder_forms']
    ];

    /**
     * Validation
     */
    public $rules = [
        'label'             => 'required',
        'name'              => 'required|alpha_dash',
        'options'           => 'required_if:type,dropdown,radio',
        'validation_regex'  => 'required_if:validation,regex',
    ];

    public $customMessages = [
        'options.required_if' => 'Options are required for dropdown and radio types.'
    ];

}