<?php namespace Bedard\FormBuilder\Models;

use DB;
use Model;

/**
 * Form Model
 */
class Form extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'bedard_formbuilder_forms';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'fields' => ['Bedard\FormBuilder\Models\Field', 'table' => 'bedard_formbuilder_fields', 'order' => 'position']
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * Validation
     */
    public $rules = [
        'name'          => 'required',
        'slug'          => 'required|between:3,64|unique:bedard_formbuilder_forms',
        'email_to'      => 'required_if:is_emailing,1'
    ];

    public $fieldNames = [];
    public function setFieldsWidgetAttribute($fieldNames)
    {
        $this->fieldNames = $fieldNames;
        return false;
    }
    public function afterSave()
    {
        if (empty($this->fieldNames))
            return;

        DB::table('bedard_formbuilder_fields')
            ->where('form_id', $this->attributes['id'])
            ->whereNotIn('name', $this->fieldNames)
            ->delete();
    }

}