<?php namespace Bedard\FormBuilder\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Forms Back-end Controller
 */
class Forms extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $bodyClass = 'compact-container';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Bedard.FormBuilder', 'formbuilder', 'forms');

        $this->addJs('/modules/backend/assets/js/october.treeview.js', 'core');
        
        $this->addCss('/plugins/bedard/formbuilder/assets/css/layout.css');
    }
}