/*
 * The form item editor.
 */
+function ($) { "use strict";
    var FormItemsEditor = function (el, options) {
        this.$el = $(el)
        this.options = options

        this.init()
    }

    FormItemsEditor.prototype.init = function() {
        var self = this;

        this.alias = this.$el.data('alias')
        this.$treevView = this.$el.find('div[data-control="treeview"]')

        this.typeInfo = {}

        // Sitemap item is clicked
        this.$el.on('open.oc.treeview', function(e) {
            return self.onItemClick(e.relatedTarget)
        })

        // Sub item is clicked in the master tabs
        this.$el.on('submenu.oc.treeview', $.proxy(this.onSubItemClick, this))
        
        // Add item click handler
        this.$el.on('click', 'a[data-control="add-item"]', function(e) {
            self.onCreateItem(e.target)
            return false
        })
    }

    /**
     * Load form properties
     */
    FormItemsEditor.prototype.loadProperties = function($popupContainer, properties) {
        this.properties = properties

        var self = this

        $.each(properties, function(property) {
            var $input = $('[name="'+property+'"]', $popupContainer).not('[type=hidden]')

            if ($input.prop('type') !== 'checkbox' ) {
                $input.val(this)
                $input.change()
            } else {
                var checked = !(this == '0' || this == 'false' || this == 0 || this == undefined || this == null)

                checked ? $input.prop('checked', 'checked') : $input.removeAttr('checked')
            }
        })
    }

    /*
     * Triggered when a sub item is clicked in the editor.
     */
    FormItemsEditor.prototype.onSubItemClick = function(e) {
        if ($(e.relatedTarget).data('control') == 'delete-form-item')
            this.onDeleteItem(e.relatedTarget)

        return false
    }

    /*
     * Removes an item
     */
    FormItemsEditor.prototype.onDeleteItem = function(link) {
        if (!confirm('Do you really want to delete this form field?'))
            return

        $(link).trigger('change')
        $(link).closest('li[data-form-item]').remove()

        $(window).trigger('oc.updateUi')

        this.$treevView.treeView('update')

        return false
    }

    /**
     * Create a new form item
     */
    FormItemsEditor.prototype.onCreateItem = function(target) {
        var itemList = $('div[data-control=treeview]').find(' > ol'),
            item = $($('script[data-item-template]', this.$el).html())

        itemList.append(item)
        this.$treevView.treeView('update')
        $(window).trigger('oc.updateUi')

        this.onItemClick(item, true)
    }

    FormItemsEditor.DEFAULTS = {
    }

    /*
     * Opens the item editor
     */
    FormItemsEditor.prototype.onItemClick = function(item, newItemMode) {
        var $item = $(item),
            $container = $('> div', $item),
            self = this

        $container.one('show.oc.popup', function(e){
            $(document).trigger('render')

            self.$popupContainer = $(e.relatedTarget)
            self.$itemDataContainer = $container.closest('li')

            $('input[type=checkbox]', self.$popupContainer).removeAttr('checked')

            self.loadProperties(self.$popupContainer, self.$itemDataContainer.data('form-item'))
            self.$popupForm = self.$popupContainer.find('form')
            self.itemSaved = false

            // Add validation to form
            self.$popupForm.data('validation', $item.data('validation'))
            self.$popupForm.data('validation-messages', $item.data('validation-messages'))

            // Show / hide the dropdown options
            $('.field-options').hide()
            $('.is-checked').hide()
            $('select[name=type]', self.$popupContainer).change(function(){
                var fieldType = $('select[name=type]').val()
                if (fieldType == 'dropdown' || fieldType == 'radio') {
                    $('.field-options').show();
                    $('.is-checked').hide();
                    $('.default-placeholder').show();
                } else if (fieldType == 'checkbox') {
                    $('.field-options').hide();
                    $('.is-checked').show();
                    $('.default-placeholder').hide();
                } else {
                    $('.field-options').hide();
                    $('.is-checked').hide();
                    $('.default-placeholder').show();
                }
            })

            // Show / hide regular expression validation
            $('.validation-regex').hide()
            $('select[name=validation]', self.$popupContainer).change(function(){
                if ($('select[name=validation]').val() == 'regex') {
                    $('.validation-regex').show();
                } else {
                    $('.validation-regex').hide();
                }
            })

            $('button[data-control="apply-btn"]', self.$popupContainer).click($.proxy(self.applyFormItem, self))
        })

        $container.one('hide.oc.popup', function(e) {
            if (!self.itemSaved && newItemMode)
                $item.remove()

            self.$treevView.treeView('update')
            self.$treevView.treeView('fixSubItems')
        })

        $container.popup({
            content: $('script[data-editor-template]', this.$el).html(),
            placement: 'center',
            modal: true,
            closeOnPageClick: true,
            highlightModalTarget: true,
            useAnimation: true,
            width: 600
        })

        return false
    }

    /**
     * Handler for apply button
     */
    FormItemsEditor.prototype.applyFormItem = function() {
    
        var self = this,
            data = {},
            propertyNames = this.$el.data('item-properties'),
            validationErrorFound = false

        // Perform validation
        var $form = this.$popupContainer.find('form')
        var validationError = $form.validate(1);
        if (validationError) {
            $.oc.flashMsg({text: validationError, 'class': 'error', 'interval': 3})
            return
        }

        $.each(propertyNames, function() {
            var propertyName = this,
                $input = $('[name="'+propertyName+'"]', self.$popupContainer).not('[type=hidden]')

            // perform validation
            if ($input.prop('type') !== 'checkbox') {
                data[propertyName] = $.trim($input.val())
            } else {
                data[propertyName] = $input.prop('checked') ? 1 : 0
            }
        })

        // Store item data
        this.$itemDataContainer.data('form-item', data);
        this.$itemDataContainer.find('input[name="FieldData[]"]').val(JSON.stringify(data));

        // Update attributes
        $.each(propertyNames, function() {
            var propertyName = this,
                $label = self.$itemDataContainer.find("[data-reference='" + propertyName + "']"),
                value = data[propertyName];

            if (propertyName == 'is_required')
                value = data['is_required'] ? '*' : '';
            
            $label.html(value);
        })

        this.itemSaved = true
        this.$popupContainer.trigger('close.oc.popup')
        this.$el.trigger('change')
    }

    // MENUITEMSEDITOR PLUGIN DEFINITION
    // ============================

    var old = $.fn.formItemsEditor

    $.fn.formItemsEditor = function (option) {
        var args = Array.prototype.slice.call(arguments, 1)
        return this.each(function () {
            var $this   = $(this)
            var data    = $this.data('oc.formitemseditor')
            var options = $.extend({}, FormItemsEditor.DEFAULTS, $this.data(), typeof option == 'object' && option)
            if (!data) $this.data('oc.formitemseditor', (data = new FormItemsEditor(this, options)))
            else if (typeof option == 'string') data[option].apply(data, args)
        })
    }

    $.fn.formItemsEditor.Constructor = FormItemsEditor

    // MENUITEMSEDITOR NO CONFLICT
    // =================

    $.fn.formItemsEditor.noConflict = function () {
        $.fn.formItemsEditor = old
        return this
    }

    // MENUITEMSEDITOR DATA-API
    // ===============

    $(document).on('render', function() {
        $('[data-control="form-item-editor"]').formItemsEditor()
    });
}(window.jQuery);