<?php namespace Bedard\FormBuilder\FormWidgets;

use Backend\Classes\FormField;
use Backend\Classes\FormWidgetBase;
use Bedard\FormBuilder\Models\Field;
use Bedard\FormBuilder\Models\Form;
use DB;

class FieldsWidget extends FormWidgetBase {

    public $defaultAlias = 'fieldswidget';

    public function widgetDetails()
    {
        return [
            'name'        => 'Fields Widgets',
            'description' => 'Adds fields to front-end forms.'
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('widget');
    }

    /**
     * Prepares widget data
     */
    private function prepareVars()
    {
        $field = new Field;
        $this->vars['attributes'] = json_encode($field->fillable);

        // Set up fields
        $this->vars['fields'] = $this->model->fields;
        $this->vars['emptyField'] = $field;

        // Prepare the item widget
        $widgetConfig = $this->makeConfig('@/plugins/bedard/formbuilder/models/field/fields.yaml');
        $widgetConfig->model = $field;
        $widgetConfig->alias = $this->alias.'Field';
        $this->vars['itemFormWidget'] = $this->makeWidget('Backend\Widgets\Form', $widgetConfig);
    }

    /**
     * {@inheritDoc}
     */
    public function loadAssets()
    {
        $this->addJs('js/fieldswidget.js', 'core');
        $this->addJs('/plugins/bedard/formbuilder/vendor/bedard/jquery-validator/validator.min.js');
        $this->addCss('css/fieldswidget.css');
    }

    /**
     * {@inheritDoc}
     */
    public function getSaveValue($value)
    {
        // Capture item data, return if none is found
        if (!$itemData = post('FieldData'))
            return FormField::NO_SAVE_DATA;

        $form = $this->model->id ? $this->model : new Form;

        $fieldNames = [];
        foreach ($itemData as $i => $field) {
            $data = json_decode($field, true);

            $field = Field::firstOrNew(['name' => $data['name']]);
            $field->position             = $i;
            $field->label                = $data['label'];
            $field->span                 = $data['span'];
            $field->comment              = $data['comment'];
            $field->type                 = $data['type'];
            $field->options              = $data['options'];
            $field->placeholder          = $data['placeholder'];
            $field->default              = $data['default'];
            $field->validation           = $data['validation'];
            $field->validation_message   = $data['validation_message'];
            $field->validation_regex     = $data['validation_regex'];
            $field->is_checked           = $data['is_checked'];
            $field->is_required          = $data['is_required'];
            $field->save();

            // Deferred binding to parent form
            $form->fields()->add($field, $this->sessionKey);
            $fieldNames[] = $field->name;
        }

        return $fieldNames;
    }
}
