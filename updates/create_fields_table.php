<?php namespace Bedard\FormBuilder\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateFieldsTable extends Migration
{

    public function up()
    {
        Schema::create('bedard_formbuilder_fields', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('form_id')->nullable()->unsigned();
            $table->integer('position')->unsigned()->default(0);
            $table->string('name')->nullable();
            $table->string('label')->nullable();
            $table->string('span', 5)->default('full');
            $table->string('comment')->nullable();
            $table->string('type')->nullable();
            $table->text('options')->nullable();
            $table->string('placeholder')->nullable();
            $table->string('default')->nullable();
            $table->string('validation')->nullable();
            $table->string('validation_regex')->nullable();
            $table->string('validation_message')->nullable();
            $table->boolean('is_checked')->default(false);
            $table->boolean('is_required')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bedard_formbuilder_fields');
    }

}
