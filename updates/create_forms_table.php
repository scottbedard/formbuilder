<?php namespace Bedard\FormBuilder\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateFormsTable extends Migration
{

    public function up()
    {
        Schema::create('bedard_formbuilder_forms', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('slug')->index();
            $table->string('description')->nullable();
            $table->string('email_to')->nullable();
            $table->string('email_from')->nullable();
            $table->string('email_cc')->nullable();
            $table->string('email_bcc')->nullable();
            $table->string('email_subject')->nullable();
            $table->text('email_body')->nullable();
            $table->boolean('is_emailing')->default(false);
            $table->boolean('is_active')->default(true);
            $table->boolean('is_private')->default(true);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bedard_formbuilder_forms');
    }

}
