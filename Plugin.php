<?php namespace Bedard\FormBuilder;

use Backend;
use System\Classes\PluginBase;

/**
 * FormBuilder Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin
     * @return  array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Form Builder',
            'description' => 'Flexible front end form management.',
            'author'      => 'Scott Bedard',
            'icon'        => 'icon-clipboard'
        ];
    }

    /**
     * Returns backend navigation
     * @return  array
     */
    public function registerNavigation()
    {
        return [
            'formbuilder' => [
                'label'         => 'Forms',
                'url'           => Backend::url('bedard/formbuilder/forms'),
                'icon'          => 'icon-clipboard',
                'permissions'   => ['bedard.formbuilder.*'],
                'order'         => 500,

                'sideMenu' => [
                    'forms' => [
                        'label'         => 'Forms',
                        'icon'          => 'icon-clipboard',
                        'url'           => Backend::url('bedard/formbuilder/forms'),
                        'permissions'   => ['bedard.formbuilder.access_forms']
                    ],
                ]
            ]
        ];
    }

}
